import { Character } from '../../src/component/character';

const seanWinter = new Character({
  name: 'Sean Winter',
  ability: {
    attack: 90,
    defense: 80,
    intelligence: 80,
    strength: 80,
    speed: 80,
    agility: 80,
  },
});
const reyValerio = new Character({
  name: 'Rey Valerio',
  ability: {
    attack: 90,
    defense: 80,
    intelligence: 80,
    strength: 80,
    speed: 80,
    agility: 80,
  },
});
console.log(seanWinter);
seanWinter.damage(60);
console.log(seanWinter.getHealth());
console.log(reyValerio);
