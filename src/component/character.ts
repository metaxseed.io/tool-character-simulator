import { GameObject } from './game-object';

export interface CharacterAbility {
  attack: number;
  defense: number;
  strength: number;
  speed: number;
  agility: number;
  intelligence: number;
}
export interface CharacterProps {
  name: string;
  ability: CharacterAbility;
  lives: number;
}
export class Character extends GameObject {
  ability: CharacterAbility;
  name: string;
  lives: number;
  constructor(props: CharacterProps) {
    super({
      name: props.name,
    });
    this.ability = props.ability;
    this.name = props.name;
    this.lives = props.lives;
  }
  public damage(attack: number) {
    const defense = this.ability.defense;
    let damage = 1;
    if (defense < attack) {
      damage = attack - this.ability.defense;
    }
    this.setHealth(this.health - damage);
  }
}
