import { Weapon, WeaponProps } from './weapon';
export interface SwordProps extends WeaponProps {
  durability: number;
  weight: number;
  strength: number;
  magic: number;
}

export class Sword extends Weapon {
  constructor(props: SwordProps) {
    super({
      defense: props.defense,
      magic: props.magic,
      name: props.name,
      power: props.power,
    });
  }
}
