import { GameObject } from '../game-object';

export interface WeaponProps {
  power: number;
  defense: number;
  magic: number;
  name: string;
}
export class Weapon extends GameObject {
  power: number;
  defense: number;
  magic: number;
  constructor(props: WeaponProps) {
    super({
      name: props.name,
    });
    this.power = props.power;
    this.defense = props.defense;
    this.magic = props.magic;
  }
}
