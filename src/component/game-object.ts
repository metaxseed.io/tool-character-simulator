export interface GameObjectProps {
  name: string;
}
export class GameObject {
  name: string;
  health: number;
  constructor(props: GameObjectProps) {
    this.name = props.name;
    this.health = 100;
  }
  public resetHealth() {
    this.health = 100;
  }
  public setHealth(value: number) {
    if (value >= 0 && value <= 100) this.health = value;
  }
  public getHealth() {
    return this.health;
  }
}
